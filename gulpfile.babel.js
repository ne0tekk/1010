import gulp from 'gulp';
import babel from 'gulp-babel';
import sass from 'gulp-sass';
import browserSync from 'browser-sync';
import sourcemaps from 'gulp-sourcemaps';
import del from 'del';

const paths = {
    styles: {
        src: ['src/styles/**/*.scss', '!src/styles/_*.scss'],
        inc: 'src/styles/_*.scss',
        dest: 'build/css/'
    },
    scripts: {
        src: ['src/scripts/**/*.js', '!src/scripts/_*.js'],
        inc: 'src/scripts/_*.js',
        dest: 'build/js/'
    },
    markup: {
        src: ['src/html/**/*.+(html|njk)', '!src/html/_*.+(html|njk)'],
        inc: 'src/html',
        dest: 'build/'
    },
    files: {
        src: ['src/**/*', '!src/styles/**/*', '!src/styles', '!src/scripts/**/*', '!src/scripts', '!src/html/**/*', '!src/html'],
        dest: 'build/'
    },
    server: {
        src: ['build/**/*.*'],
        dest: 'build/'
    }
};

export const clean = () => del(['build']);

export function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.styles.dest));
};

export function scripts() {
    return gulp.src(paths.scripts.src)
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.scripts.dest));
};

export function markup() {
    return gulp.src(paths.markup.src)
        .pipe(gulp.dest(paths.markup.dest));
};

export function files() {
    return gulp.src(paths.files.src)
        .pipe(gulp.dest(paths.files.dest));
};

export function server() {
    browserSync.create().init({
        notify: false,
        server: {
            baseDir: paths.server.dest
        },
        files: paths.server.src
    });
};

function watchAll() {
    gulp.watch(paths.styles.src, styles);
    gulp.watch(paths.styles.inc, styles);
    gulp.watch(paths.scripts.src, scripts);
    gulp.watch(paths.scripts.inc, scripts);
    gulp.watch(paths.markup.src, markup);
    gulp.watch(paths.markup.inc, markup);
    gulp.watch(paths.files.src, files);
}

export { watchAll as watch };

const build = gulp.series(clean, gulp.parallel(files, styles, scripts, markup));

export { build as build };

const serve = gulp.parallel(watchAll, gulp.series(build, server));

export default serve;
