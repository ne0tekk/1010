const canvas = document.getElementById("canvas"),
  bg = document.getElementById("bg");
const ctx = canvas.getContext("2d"),
  grid = bg.getContext("2d");
const BLOCK_W = 30;
const BLOCK_H = 30;

/**
 * Рисование прямоугольного блока на холсте в заданных координатах
 * @param {int} x Координата X для левого верхнего угла блока на холсте
 * @param {int} y Координата Y для левого верхнего угла блока на холсте
 * @param {string} color_fill Цвет заливки блока
 * @param {string} color_stroke Цвет обводки блока
 */
function drawRect(x, y, color_fill, color_stroke) {
  ctx.fillStyle = color_fill;
  ctx.strokeStyle = color_stroke;
  ctx.fillRect(x, y, BLOCK_W, BLOCK_H);
  ctx.strokeRect(x, y, BLOCK_W, BLOCK_H);
}

/**
 * Отрисовка сетки в заданных координатах на холсте
 * @param {int} x Координата X на холсте для левого верхнего угла сетки
 * @param {int} y Координата Y на холсте для левого врехнего угла сетки
 */
function drawGrid(x, y) {
  let data = `
    <svg width="${BLOCK_W * 10 + 1}px" height="${BLOCK_H * 10 +
    1}px" xmlns="http://www.w3.org/2000/svg">
        <defs>
            <pattern id="grid" width="${BLOCK_W}" height="${BLOCK_H}" patternUnits="userSpaceOnUse">
                <path d="M ${BLOCK_W} 0 L 0 0 0 ${BLOCK_H}" fill="none" stroke="gray" stroke-width="0.5" />
            </pattern>
        </defs>
        <rect width="100%" height="100%" fill="url(#grid)" />
    </svg>
    `;

  let DOMURL = window.URL || window.webkitURL || window;

  let img = new Image();
  let svg = new Blob([data], { type: "image/svg+xml;charset=utf-8" });
  let url = DOMURL.createObjectURL(svg);

  img.onload = function() {
    grid.drawImage(img, x, y);
    DOMURL.revokeObjectURL(url);
  };
  img.src = url;
}
