const offsetX = canvas.getBoundingClientRect().left;
const offsetY = canvas.getBoundingClientRect().top;

function mouseMove(e) {
  if (game.isDragging) {
    e.preventDefault();
    e.stopPropagation();

    let mouseX = parseInt(e.clientX - offsetX);
    let mouseY = parseInt(e.clientY - offsetY);

    let dx = mouseX - game.startX;
    let dy = mouseY - game.startY;

    for (let i = 0; i < game.stock.length; ++i) {
      let objects = game.stock[i].objects;
      for (let j = 0; j < objects.length; ++j) {
        let shape = objects[j];
        if (game.stock[i].isMoving) {
          shape.x += dx;
          shape.y += dy;
        }
      }
    }

    draw();

    game.startX = mouseX;
    game.startY = mouseY;
  }
}

function mouseUp(e) {
  e.preventDefault();
  e.stopPropagation();

  game.isDragging = false;

  let idx = false;
  game.stock.map((shape, index) => {
    if (shape.isMoving) {
      if (!settleShape(shape.objects)) {
        shape.objects = game.initialObj;
        shape.isMoving = false;
      } else {
        idx = index;
        game.score += shape.objects.length;
        checkForClear();
        checkForLose();
      }
    }
  });

  if (idx !== false) {
    game.stock.splice(idx, 1);
  }

  if (game.stock.length === 0) {
    generateStock(0, 400);
    checkForLose();
  }

  game.initialObj = [];

  draw();
}

function mouseDown(e) {
  e.preventDefault();
  e.stopPropagation();

  let mouseX = parseInt(e.clientX - offsetX);
  let mouseY = parseInt(e.clientY - offsetY);

  game.stock.sort((a, b) => b.zIndex - a.zIndex);

  loop: for (let i = 0; i < game.stock.length; ++i) {
    let objects = JSON.parse(JSON.stringify(game.stock[i].objects));
    for (let j = 0; j < objects.length; ++j) {
      let shape = objects[j];
      if (
        mouseX > shape.x - 1 &&
        mouseX < shape.x + shape.w + 1 &&
        mouseY > shape.y - 1 &&
        mouseY < shape.y + shape.h + 1
      ) {
        game.isDragging = true;
        game.stock[i].isMoving = true;

        game.initialObj = objects;

        game.stock.map((value, index) => {
          if (value.zIndex > game.stock[i].zIndex) {
            value.zIndex--;
          }
        });

        game.stock[i].zIndex = game.stock.length;

        break loop;
      }
    }
  }

  game.startX = mouseX;
  game.startY = mouseY;
}
