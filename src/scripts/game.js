const game = {
  isDragging: false,
  startX: null,
  startY: null,
  board: {
    x: 0,
    y: 0,
    matrix: [],
  },
  shapes: [
    // bars
    [0, 1, 2, 3, 4],
    [0, 1, 2, 3],
    [0, 1, 2],
    [0, 1],
    [0],
    // corners
    [0, 1, 2, 5, 10],
    [0, 1, 5],
    // squares
    [0, 1, 2, 5, 6, 7, 10, 11, 12],
    [0, 1, 5, 6],
    // ts
    [0, 1, 2, 6],
  ],
  stock: [],
  initialObj: [],
  score: 0,
};

function start() {
  generateBoard((canvas.width - BLOCK_W * 10) / 2 - 1, 10);
  generateStock(0, 400);

  canvas.onmousedown = mouseDown;
  canvas.onmouseup = mouseUp;
  canvas.onmousemove = mouseMove;

  drawGrid(game.board.x, game.board.y);

  draw();
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  game.board.matrix.map(cell => {
    if (cell.value === 1) {
      drawRect(cell.x, cell.y, "#ccc", "#2e2e2e");
    }
  });

  game.stock.sort((a, b) => a.zIndex - b.zIndex);

  for (let i = 0; i < game.stock.length; ++i) {
    drawObj(game.stock[i].objects);
  }
}

window.onload = start;
