function output(matrix) {
  console.log("");
  console.log("---matrix output start---");
  let n = matrix.length;
  for (let i = 0; i < n; ++i) {
    let row = [];
    for (let j = 0; j < n; ++j) {
      row.push(matrix[i][j]);
    }
    console.log(row);
  }
  console.log("---matrix output end---");
  console.log("");
}

function generateBoard(x, y) {
  game.board.x = x;
  game.board.y = y;

  var kx = 0,
    ky = game.board.y;
  for (let i = 0; i < 100; ++i) {
    game.board.matrix.push({
      x: game.board.x + kx * BLOCK_W,
      y: ky,
      w: BLOCK_W,
      h: BLOCK_H,
      value: 0,
    });

    if ((i + 1) % 10 === 0) {
      kx = 0;
      ky += BLOCK_H;
    } else {
      kx++;
    }
  }
}

function getRandMatrix() {
  let shapeIdx = Math.floor(Math.random() * game.shapes.length),
    rotateIdx = Math.floor(Math.random() * 4);

  let matrix = generateMatrix(game.shapes[shapeIdx]);

  rotateMatrix(matrix, rotateIdx);

  return matrix;
}

function generateStock(x, y) {
  var kx = x,
    ky = y,
    obj_container = BLOCK_W * 5,
    margin = (canvas.width - 3 * obj_container) / 4;

  for (let i = 0; i < 3; ++i) {
    let matrix = getRandMatrix();
    let obj = [];

    kx += margin;

    var dy = false;
    for (let j = 0; j < matrix.length; ++j) {
      let row = matrix[j];

      if (dy) {
        ky += BLOCK_H;
        dy = false;
      }
      for (let k = 0; k < row.length; ++k) {
        if (row[k] === 1) {
          dy = true;
          obj.push({
            x: kx + BLOCK_W * k,
            y: ky,
            w: BLOCK_W,
            h: BLOCK_H,
          });
        }
      }
    }

    game.stock.push({
      isMoving: false,
      objects: obj,
      matrix: matrix,
      zIndex: i + 1,
    });

    obj = [];

    kx += obj_container;
    ky = y;
  }
}

function generateMatrix(shape) {
  let matrix = [],
    row = [];

  for (let i = 0; i < 25; ++i) {
    if (shape.indexOf(i) >= 0) {
      row.push(1);
    } else {
      row.push(0);
    }
    if ((i + 1) % 5 === 0) {
      matrix.push(row);
      row = [];
    }
  }

  return matrix;
}

function rotateCW(matrix) {
  let n = matrix.length;

  for (let i = 0; i < n / 2; ++i) {
    for (let j = i; j < n - i - 1; ++j) {
      let t = matrix[i][j];

      matrix[i][j] = matrix[n - 1 - j][i];
      matrix[n - 1 - j][i] = matrix[n - 1 - i][n - 1 - j];
      matrix[n - 1 - i][n - 1 - j] = matrix[j][n - 1 - i];
      matrix[j][n - 1 - i] = t;
    }
  }

  return matrix;
}

function rotateMatrix(matrix, deg) {
  for (let i = 0; i < deg; ++i) {
    rotateCW(matrix);
  }

  return matrix;
}

function drawObj(obj) {
  for (let i = 0; i < obj.length; ++i) {
    drawRect(obj[i].x, obj[i].y, "#ccc", "#2e2e2e");
  }
}

function settleShape(shape) {
  let result = true,
    idx = [];

  for (let i = 0; i < shape.length; ++i) {
    let point = {
      x: shape[i].x + shape[i].w / 2,
      y: shape[i].y + shape[i].h / 2,
    };

    for (let j = 0; j < game.board.matrix.length; ++j) {
      let cell = game.board.matrix[j];
      if (
        point.x > cell.x &&
        point.x < cell.x + cell.w &&
        point.y > cell.y &&
        point.y < cell.y + cell.h &&
        cell.value === 0
      ) {
        idx.push(j);
        result = true;
        break;
      } else {
        result = false;
      }
    }

    if (!result) {
      break;
    }
  }

  if (result) {
    idx.map(val => {
      game.board.matrix[val].value = 1;
    });
  }

  return result;
}

function checkForClear() {
  let data = getColsRows();

  data.cols.map((arr, index) => {
    if (arr.filter(value => value === 0).length === 0) {
      clearCol(index);
      game.score += arr.length;
    }
  });

  data.rows.map((arr, index) => {
    if (arr.filter(value => value === 0).length === 0) {
      clearRow(index);
      game.score += arr.length;
    }
  });
}

function checkForLose() {
  // надо проверить всё по стоку на возможность установки на каждое свободное поле
  // сначала в цикле перебираем все свободные поля и затем пытаемся инициировать возможность
  // установки туда фигуры
  // если все фигуры из стока не могут быть поставлены, это проигрыш
}

function getColsRows() {
  let result = {
    cols: [],
    rows: [],
  };

  for (let i = 0; i < 10; ++i) {
    result.cols[i] = [];
    result.rows[i] = [];
    for (let j = 0; j < 10; ++j) {
      result.cols[i][j] = 0;
      result.rows[i][j] = 0;
    }
  }

  let j = 0,
    k = 0;
  for (let i = 0; i < game.board.matrix.length; ++i) {
    let cell = game.board.matrix[i];

    result.rows[j][k] = cell.value;
    result.cols[k][j] = cell.value;

    k++;

    if ((i + 1) % 10 === 0) {
      k = 0;
      j++;
    }
  }

  return result;
}

function clearRow(index) {
  let start = index * 10;
  for (let i = start; i < start + 10; ++i) {
    game.board.matrix[i].value = 0;
  }
}

function clearCol(index) {
  for (let i = index; i < 100 - 10 + index + 1; i += 10) {
    game.board.matrix[i].value = 0;
  }
}
