function output(matrix) {
  let n = matrix.length;
  for (let i = 0; i < n; ++i) {
    let row = [];
    for (let j = 0; j < n; ++j) {
      row.push(matrix[i][j]);
    }
    console.log(row);
  }
}

function rotateCCW(matrix) {
  let n = matrix.length;

  for (let i = 0; i < n / 2; ++i) {
    for (let j = i; j < n - i - 1; ++j) {
      let t = matrix[i][j];

      matrix[i][j] = matrix[j][n - 1 - i];
      matrix[j][n - 1 - i] = matrix[n - 1 - i][n - 1 - j];
      matrix[n - 1 - i][n - 1 - j] = matrix[n - 1 - j][i];
      matrix[n - 1 - j][i] = t;
    }
  }

  return matrix;
}

function rotateCW(matrix) {
  let n = matrix.length;

  for (let i = 0; i < n / 2; ++i) {
    for (let j = i; j < n - i - 1; ++j) {
      let t = matrix[i][j];

      matrix[i][j] = matrix[n - 1 - j][i];
      matrix[n - 1 - j][i] = matrix[n - 1 - i][n - 1 - j];
      matrix[n - 1 - i][n - 1 - j] = matrix[j][n - 1 - i];
      matrix[j][n - 1 - i] = t;
    }
  }

  return matrix;
}

function rotate180(matrix) {
  function reverse(matrix) {
    let n = matrix.length;
    for (let i = 0; i < n; ++i) {
      for (let j = 0, k = n - 1; j < k; ++j, --k) {
        let t = matrix[j][i];
        matrix[j][i] = matrix[k][i];
        matrix[k][i] = t;
      }
    }
  }

  function transpose(matrix) {
    let n = matrix.length;
    for (let i = 0; i < n; ++i) {
      for (let j = i; j < n; ++j) {
        let t = matrix[i][j];
        matrix[i][j] = matrix[j][i];
        matrix[j][i] = t;
      }
    }
  }

  transpose(matrix);
  reverse(matrix);
  transpose(matrix);
  reverse(matrix);

  return matrix;
}

function rotate(matrix, deg) {
  for (let i = 0; i < deg; ++i) {
    rotateCW(matrix);
  }

  return matrix;
}

document.getElementById("button").onclick = function() {
  let matrix = [
    [1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
  ];

  let deg = Math.floor(Math.random() * 4);
  output(rotate(matrix, deg));
  console.log("");
};
